# coding=utf-8

from reportlab.pdfgen import canvas


def hello(c):
    c.drawString(100, 100, 'Hello from Canvas!')

c = canvas.Canvas('hello.pdf')
hello(c)
c.showPage()
c.save()
